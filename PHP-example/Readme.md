This example is based on the bot-api-base one. See https://github.com/tg-bot-api/bot-api-base 

# License
![GPL logo](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)
This work is under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
