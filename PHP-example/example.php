<?php
require ('vendor/tg-bot-api/bot-api-base/');

use \TgBotApi\BotApiBase\ApiClient;
use \TgBotApi\BotApiBase\BotApi;
use \TgBotApi\BotApiBase\BotApiNormalizer;
use \TgBotApi\BotApiBase\Method\SendMessageMethod;

$botKey = '<bot key>';

$requestFactory = new Http\Factory\Guzzle\RequestFactory();
$streamFactory = new Http\Factory\Guzzle\StreamFactory();
$client = new Http\Adapter\Guzzle6\Client();

$apiClient = new ApiClient($requestFactory, $streamFactory, $client);
$bot = new BotApi($botKey, $apiClient, new BotApiNormalizer());

$userId = '<user id>';

$bot->send(SendMessageMethod::create($userId, 'Hi'));

?>
