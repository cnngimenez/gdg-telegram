This example is based on the one provided by the telegram package.

# License
![GPL logo](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)
This work is under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
