

Telebot: https://github.com/ba0f3/telebot.nim

Install `nimble install telebot`.

Compile with `nim c -d:ssl echobot.nim`.
Create a `secret.key` file and paste the bot token there.

# License
![GPL logo](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)
This work is under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
