  

The example.rkt and teleracket.rkt are based on

Teleracket: https://github.com/2-3/teleracket


There is another library: https://github.com/profan/teleracket

# License
![GPL logo](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)
This work is under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
